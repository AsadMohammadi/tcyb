FILE = Journal
SUP = supplement

all: $(FILE).tex
	latex $(SUP)
	latex $(FILE)
	bibtex $(FILE)
	latex $(FILE)
	latex $(FILE)

sup: $(SUP).tex
	latex $(SUP)
	bibtex $(SUP)
	latex $(SUP)
	latex $(SUP)

pdf: all
	dvips -o $(FILE).ps $(FILE).dvi
	ps2pdf $(FILE).ps
	dvips -o $(SUP).ps $(SUP).dvi
	ps2pdf $(SUP).ps

clean:
	rm -rf *.toc *.bbl *.aux *.blg *.log *.dvi $(FILE).ps $(SUP).ps

cleanall: clean
	rm -rf $(FILE).pdf $(SUP).pdf
