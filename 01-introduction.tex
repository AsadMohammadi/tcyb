\section{Introduction}
Evolutionary Multi-objective Optimization (EMO) has been successfully
applied to many real-world problems~\cite{CoelloApps}. However, finding
uniformly distributed solutions in the objective space remains a challenge
for existing EMO algorithms. Two major difficulties of finding a uniform
set of solutions on the Pareto front are as follows. Firstly, an accurate
metric is needed to measure the uniformity of solutions in the objective
space.  Secondly, most of the existing metrics measure the uniformity of
the entire solution set, but they are not able to rank the individuals in
terms of their contributions to the overall uniformity.

Several metrics such as Crowding Distance~\cite{NSGA-II}, Minimum Spanning Tree (MST)~\cite{MST}, 
%and Maximum Spread~\cite{Z-thesis}
$k$-nearest neighbor~\cite{silverman1986density}, and Harmonic
Distance~\cite{harmonic-dis} have been proposed to address
the uniformity issue. However, most of these metrics are not
scalable to solve many-objective problems with more than three
objectives~\cite{ManyConflictingObjectives,Many-once-one-many}.  Maintaining
diversity of solutions becomes even more difficult in many-objective problems
since the size of the search space grows exponentially. Unfortunately, the
accuracy of current metrics degrade severely in high dimensional spaces. An
increase in the number of objectives increases the probability of two
random solutions become non-dominated to each other.  If the majority
or all of the solutions become non-dominated, which is very likely in
high dimensional spaces, the convergence property of Pareto-dominance
algorithms will be severely affected~\cite{ManyConflictingObjectives,
ishibuchi2009MOEA/DVsR-NSGA-II}.

Different approaches have been suggested to improve the scalability
issue of Pareto dominance EMO approaches~\cite{Ishibuchi2008,
ishibuchi2008effectiveness, GrEA, PICEA-g, AGE-II, SDE,
two_arch2}.~\citet{ishibuchi2008effectiveness} suggested five approaches
to increase selection pressure towards the Pareto front by introducing
different mechanisms for ranking non-dominated solutions, modifying the
dominance relationship to decrease the number of non-dominated solutions in
a population, modifying the objective functions to increase the correlation
among them, modifying the density estimators, and hybridization with 
local search~\cite{jaszkiewicz2002performance}. It has
been empirically shown that the above approaches improve the convergence
of solutions at the expense of losing diversity~\cite{corne2007techniques}.

To overcome such problems, in this paper, we propose to adopt two strategies:
\begin{enumerate}
\item Eliminating dominance ranking by using decomposition techniques.
\item Confining the search space and focusing on specific parts of the
      Pareto-optimal front instead of approximating the entire Pareto-optimal
      front, which helps to reduce the computational cost.
\end{enumerate}
A promising alternative to Pareto-dominance algorithms is to use
decomposition methods to convert a multi-objective problem into a
set of single objective problems. Various scalarizing (aggregation)
functions exist including Weighted-Sum~\cite{Miettinen2001a},
Tchebycheff~\cite{Miettinen2001a}, Boundary Intersection~\cite{NBI},
and Penalty-based Boundary Intersection~\cite{moead}. These decomposition
methods use different scalarizing functions that assign a set of weights to
the objective functions. Solving the resultant single objective problem with
various weight values results in obtaining solutions on different parts
of the Pareto front. This makes the decomposition-based EMO algorithms
less sensitive to the selection pressure issue.  MOEA/D~\cite{moead}
and MOGLS~\cite{MOGLS} are the most popular EMO algorithms that eliminate
dominance ranking by employing scalarizing functions.  Two main reasons that
we use decomposition methods in this research are as follows.  
\begin{enumerate}
\item The decomposition methods provide an explicit means of controlling
where a solution converges by changing the weight vectors. This can be
used to control the distribution of solutions directly.

\item Weight vectors also help in handling user-preference by guiding
solutions towards the preferred region.
\end{enumerate}

Adopting user-preference based approaches is another promising way
of alleviating the scalability issue of optimizing many-objective
problems. User-preference based approaches focus on a smaller region of
the Pareto front, so they need less computational resources as compared
to approaches that approximate the entire Pareto front. There are
various types of user-preference methods including \emph{a priori},
\emph{interactive}, and \emph{a posteriori} decision making. Some of the
popular user-preference based EMO algorithms are R-NSGA-II~\cite{rnsga-ii},
RD-NSGA-II~\cite{rd-rnsgaii}, LBS-NSGA-II~\cite{lightbeam}, and
PICEA~\cite{PICEA}.  R-NSGA-II~\cite{rnsga-ii} uses reference points to
incorporate the user preferences while RD-NSGA-II~\cite{rd-rnsgaii} uses
reference directions for the same purpose.  LBS-NSGA-II~\cite{lightbeam}
is another algorithm that uses a light beam approach to incorporate
user preferences. Finally, PICEA~\cite{PICEA} is an example of \emph{a
posteriori} decision making. Since applying scalarization techniques
to some extent alleviate the selection pressure issue of dominance-based
approaches, and utilizing user preference information reduces computational
cost, combining these two methods is a promising strategy to solve
many-objective optimization problems~\cite{rmead}. R-MEAD~\cite{rmead},
r-MOEA/D-STM~\cite{moead-stm} and R-MEAD2~\cite{RMEAD2} are such
algorithms. 

As was previously mentioned, one of the main features of decomposition
methods is controlling the distribution of solutions by adjusting weight
vectors. The following question arises when dealing with decomposition
methods: How to generate a set of weight vectors for a given aggregation
function in order to ensure a desired level of diversity among the solutions?
UMOEA/D~\cite{umoead} replaces simplex-lattice design method, which is
used in MOEA/D~\cite{moead} to initialize the weight vector, with the good
lattice point (GLP) method. The study shows that since GLP can generate
more uniform weight vectors than simplex-lattice design, UMOEA/D can
generate more uniform solutions than MOEA/D. MOEA/D-AWA~\cite{moead-awa}
enhanced MOEA/D in two aspects: the weight vector initialization method,
and the mechanism of updating the weight vectors during the search
process. MOEA/D-UDM~\cite{moead-udm}, which uses a more complex
strategy to initialize the weight vector, suggested a weight vector
initialization method based on uniform design measurement, and tried
to remove non-linearity between weight vectors and optimal solutions of
the sub-problems.~\citet{RMEAD2} replaced the simplex-lattice design as a
weight vector initialization with a simple random generator (RNG). It has
been shown that RNG can generate more uniform weights than GLP, especially
on many-objective problems. Bi-criterion evolution(BCE)~\cite{BCE} is
another decomposition-based EMO that attempts to maintain the diversity
of solutions. BCE uses two populations namely PC (Pareto Criterion)
and NPC (Non-Pareto Criterion) where NPC guides the search towards the
optimal front while PC mainly focus to maintain diversity of solutions by
exploring undeveloped or not well-developed regions in the objective space.
These two populations communicate with each other and use the suitable
individuals generated by either of them.

Most of the above algorithms either use sophisticated methods to generate a
set of well distributed weight vector, or use the Pareto front information
for manipulating the weight vectors to generate a uniform set of solutions in
the objective space. It should be noted that the relationship between the
weight space and the objective space is not always linear. In other words,
a uniform set of weight vectors does not necessarily map to a uniform set
of solutions in the objective space. It is possible that a uniform set of
weight vectors generate very non-uniform solutions in the objective space,
or conversely a non-uniform set of weight vectors generate uniform solutions
in the objective space. Relying on Pareto-optimal front information to
engineer weight vectors to obtain a uniform set of solutions is also not
viable since this information is not always available. As a result, there
is a need for establishing a feedback mechanism during the course of optimization to
assess the uniformity of solutions in the objective space, and to use the feedback 
to dynamically adapt the weights to further improve the uniformity of solutions.
% maintain this unique relationship between the weight vectors and solutions
% in objective space.  In other words, weight vectors should be dynamically
% updated during the course of optimization with the aim of generating uniform
% solutions in the objective space. To achieve this, a feedback mechanism
% is proposed in this paper. In order to do so, some existing metrics have
% been used to measure the uniformity of solutions.
% In this study, we use two existing metrics called Overlapping
% Hypervolume (OHV)~\cite{ohv} and Potential Energy (PE)~\cite{pe}.
% Since the computational complexity of these two metrics is relatively
% low, the calculation of potential energy is not affected by the shape of
% boundary and it agrees with human perception of global uniformity, they
% turn out to be a suitable choice for this work. A new uniformity metric
% is also proposed called Potential Energy with direction vector (PEV).
% It should be noted that the proposed feedback mechanism approach can be
% applied to both user-preference and non-user preference decomposition
% algorithms. Since the focus of this paper is many-objective problems,
% the proposed feedback mechanism will be only applied here to show that
% a user-preference approach can be used to reduce the computational cost.
% As a result, the proposed approach should satisfy the preference of a user
% as well as uniformity of solutions.

In this study, we propose three different ways of devising such a feedback
mechanism in order to improve the uniformity of the solutions.	This is done
in a framework which we call UR-MEAD2 that can adopt any uniformity metric
as a feedback mechanism.  An advantage of UR-MEAD2 is that it can work
with any decomposition method such as Weighted-Sum~\cite{Miettinen2001a},
Tchebycheff~\cite{Miettinen2001a}, Boundary Intersection~\cite{NBI}, or
Penalty-based Boundary Intersection (PBI)~\cite{moead}.  It should also
be noted that the proposed methods can be applied to both user-preference
and non-user preference decomposition algorithms. However, in this paper
the emphasis is on the incorporation of user-preference information for
the sake of reducing the computational cost when tackling many-objective
problems. More specifically the main contributions of this work can be
summarized as follows:
\begin{inparaenum}
\item Two existing metrics namely, Overlapping Hypervolume (OHV)~\cite{ohv}
      and Potential Energy (PE)~\cite{pe} are modified to allow ranking of
      the individuals in terms of their contribution towards
      improving the overall uniformity. This is then used as a feedback
      to update the weights to improve the overall uniformity.
\item A new uniformity metric called Potential Energy with direction vector
      (PEV) is proposed that can measure the amount of contribution
      that each solution makes to improve (or degrade) the uniformity. This
      feature of PEV provides some gradient information to the algorithm
      in order to update the weight vectors in a meaningful way which results in
      more uniform solutions in the objective space.
\end{inparaenum}
% An advantage of UR-MEAD2 is that it can work with any decomposition method
% such as Weighted-Sum~\cite{Miettinen2001a}, Boundary Intersection~\cite{NBI},
% or Penalty-based Boundary Intersection (PBI)~\cite{moead}.  The performance
% of decomposition methods (Tchebycheff and PBI with different penalty factors)
% under each feedback mechanism method is compared, and the experimental
% results show that Tchebycheff which uses our proposed uniformity metric
% (PEV) as a feedback mechanism has the best performance and outperforms a
% state-of-the-art algorithm (R-NSGA-II~\cite{rnsga-ii}).

The rest of the paper is organized as follows.  Section~\ref{sec:background}
introduces the background on decomposition methods.  In
Section~\ref{sec:litrature}  a brief survey of previous studies
is given.  The proposed feedback mechanism algorithm is described in
section~\ref{sec:proposedApproach}. Section~\ref{sec:experimnetalResult}
discusses the experimental results.  Finally Section~\ref{sec:conclusion}
summarizes the research findings, and possible future research directions.